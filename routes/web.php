<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/', 'BookController@index')->name('home');
Route::post('/books/{book}/buy', 'TransactionController@buyBook');
Route::post('/transactions/add_balance', 'TransactionController@addBalance');
Route::get('/transactions', 'TransactionController@index');
Route::get('/users', 'TransactionController@showUsers');

Route::resource('books', 'BookController')->except('index');
Route::resource('authors', 'AuthorController')->except('show');

