<?php

use Illuminate\Database\Seeder;

class TransactionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('transactions')->delete();
        
        \DB::table('transactions')->insert(array (
            0 => 
            array (
                'id' => 1,
                'user_id' => 1,
                'book_id' => NULL,
                'amount' => 1,
                'created_at' => '2019-11-24 18:42:10',
                'updated_at' => '2019-11-24 18:42:10',
            ),
            1 => 
            array (
                'id' => 2,
                'user_id' => 1,
                'book_id' => NULL,
                'amount' => 500,
                'created_at' => '2019-11-24 18:42:19',
                'updated_at' => '2019-11-24 18:42:19',
            ),
            2 => 
            array (
                'id' => 3,
                'user_id' => 3,
                'book_id' => NULL,
                'amount' => 300,
                'created_at' => '2019-11-24 18:42:29',
                'updated_at' => '2019-11-24 18:42:29',
            ),
            3 => 
            array (
                'id' => 4,
                'user_id' => 4,
                'book_id' => NULL,
                'amount' => 50,
                'created_at' => '2019-11-24 18:42:35',
                'updated_at' => '2019-11-24 18:42:35',
            ),
            4 => 
            array (
                'id' => 5,
                'user_id' => 1,
                'book_id' => 1,
                'amount' => NULL,
                'created_at' => '2019-11-24 18:42:56',
                'updated_at' => '2019-11-24 18:42:56',
            ),
            5 => 
            array (
                'id' => 6,
                'user_id' => 1,
                'book_id' => 3,
                'amount' => NULL,
                'created_at' => '2019-11-24 18:43:14',
                'updated_at' => '2019-11-24 18:43:14',
            ),
            6 => 
            array (
                'id' => 7,
                'user_id' => 1,
                'book_id' => 7,
                'amount' => NULL,
                'created_at' => '2019-11-24 18:43:23',
                'updated_at' => '2019-11-24 18:43:23',
            ),
            7 => 
            array (
                'id' => 8,
                'user_id' => 1,
                'book_id' => 9,
                'amount' => NULL,
                'created_at' => '2019-11-24 18:43:37',
                'updated_at' => '2019-11-24 18:43:37',
            ),
            8 => 
            array (
                'id' => 9,
                'user_id' => 4,
                'book_id' => 10,
                'amount' => NULL,
                'created_at' => '2019-11-24 18:45:23',
                'updated_at' => '2019-11-24 18:45:23',
            ),
            9 => 
            array (
                'id' => 10,
                'user_id' => 4,
                'book_id' => 10,
                'amount' => NULL,
                'created_at' => '2019-11-24 18:46:40',
                'updated_at' => '2019-11-24 18:46:40',
            ),
            10 => 
            array (
                'id' => 11,
                'user_id' => 4,
                'book_id' => NULL,
                'amount' => 40,
                'created_at' => '2019-11-24 18:47:09',
                'updated_at' => '2019-11-24 18:47:09',
            ),
            11 => 
            array (
                'id' => 12,
                'user_id' => 3,
                'book_id' => 8,
                'amount' => NULL,
                'created_at' => '2019-11-24 18:49:19',
                'updated_at' => '2019-11-24 18:49:19',
            ),
            12 => 
            array (
                'id' => 13,
                'user_id' => 3,
                'book_id' => 1,
                'amount' => NULL,
                'created_at' => '2019-11-24 18:49:48',
                'updated_at' => '2019-11-24 18:49:48',
            ),
            13 => 
            array (
                'id' => 14,
                'user_id' => 3,
                'book_id' => NULL,
                'amount' => 33,
                'created_at' => '2019-11-24 18:50:06',
                'updated_at' => '2019-11-24 18:50:06',
            ),
            14 => 
            array (
                'id' => 15,
                'user_id' => 1,
                'book_id' => NULL,
                'amount' => 10,
                'created_at' => '2019-11-24 18:50:15',
                'updated_at' => '2019-11-24 18:50:15',
            ),
        ));
        
        
    }
}