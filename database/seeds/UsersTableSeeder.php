<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Ivan',
                'phone' => '00385123456',
                'email' => 'test@test.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$N1WC6ZdcIOMLN5P9/laWSuUaoaanNPsWvqAYAkEKntS0E47iUS46q',
                'balance' => 44257,
                'is_admin' => 0,
                'remember_token' => NULL,
                'created_at' => '2019-11-24 17:26:39',
                'updated_at' => '2019-11-24 18:50:15',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'John Doe',
                'phone' => '00341234567',
                'email' => 'admin@admin.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$DQMOYUaapf1eaEspDWbHlefzSbK7nNwt4tJqhuVkD18VJ.Z46jCHe',
                'balance' => 0,
                'is_admin' => 1,
                'remember_token' => NULL,
                'created_at' => '2019-11-24 17:28:32',
                'updated_at' => '2019-11-24 17:28:32',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Maria Lopez',
                'phone' => '00381234567',
                'email' => 'test2@test.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$MvTaAJtb1C8E08nRQJu.Wu/xmDXZHC1bGCooRjVjxTD6foChxU59y',
                'balance' => 30020,
                'is_admin' => 0,
                'remember_token' => NULL,
                'created_at' => '2019-11-24 17:29:59',
                'updated_at' => '2019-11-24 18:50:06',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Marco Sena',
                'phone' => '00123456789',
                'email' => 'test3@test.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$Te2RlcCvP4Z45zc/Eux4hubORt6LiySYP.EkqyDgBQHp2Me36.6a.',
                'balance' => 5945,
                'is_admin' => 0,
                'remember_token' => NULL,
                'created_at' => '2019-11-24 17:31:04',
                'updated_at' => '2019-11-24 18:47:09',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Leonardo Pochen',
                'phone' => '00123495841',
                'email' => 'test4@test.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$xgyp5p.weKIV3R/7I6pxBe8cPomPecLNs10xnmjIV8siaIAjXUx6.',
                'balance' => 0,
                'is_admin' => 0,
                'remember_token' => NULL,
                'created_at' => '2019-11-24 17:32:37',
                'updated_at' => '2019-11-24 17:32:37',
            ),
        ));
        
        
    }
}