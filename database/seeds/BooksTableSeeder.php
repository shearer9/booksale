<?php

use Illuminate\Database\Seeder;

class BooksTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('books')->delete();
        
        \DB::table('books')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'The Lord of the Rings',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem beatae eaque ex in minima molestiae perspiciatis placeat sint vero. Excepturi expedita in saepe sapiente, vitae voluptatem? Beatae eum, molestias. Minima!',
                'author_id' => 1,
                'price' => 1500,
                'stock' => 3,
                'book_image' => '/images/books/the-lord-of-the-rings_1574617288.jpg',
                'created_at' => '2019-11-24 17:41:28',
                'updated_at' => '2019-11-24 18:49:48',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Two Towers',
                'description' => 'The story begins in the Shire, where the hobbit Frodo Baggins inherits the Ring from Bilbo Baggins, his cousin[c] and guardian. Neither hobbit is aware of the Ring\'s nature, but Gandalf the Grey, a wizard and an old friend of Bilbo, suspects it to be Sauron\'s Ring. Seventeen years later, after Gandalf confirms his guess, he tells Frodo the history of the Ring and counsels him to take it away from the Shire. Frodo sets out, accompanied by his gardener, servant and friend, Samwise "Sam" Gamgee, and two cousins, Meriadoc "Merry" Brandybuck and Peregrin "Pippin" Took. They are nearly caught by the Black Riders, but shake off their pursuers by cutting through the Old Forest. There they are aided by Tom Bombadil, a strange and merry fellow who lives with his wife Goldberry in the forest.

The hobbits reach the town of Bree, where they encounter a Ranger named Strider, whom Gandalf had mentioned in a letter. Strider persuades the hobbits to take him on as their guide and protector. Together, they leave Bree after another close escape from the Black Riders. On the hill of Weathertop, they are again attacked by the Black Riders, who wound Frodo with a cursed blade. Strider fights them off and leads the hobbits towards the Elven refuge of Rivendell. Frodo falls deathly ill from the wound. The Black Riders nearly capture him at the Ford of Bruinen, but flood waters summoned by Elrond, master of Rivendell, rise up and overwhelm them.',
                'author_id' => 1,
                'price' => 2000,
                'stock' => 6,
                'book_image' => '/images/books/the-fellowship-of-the-ring_1574618043.jpg',
                'created_at' => '2019-11-24 17:46:46',
                'updated_at' => '2019-11-24 17:55:55',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'The Return of the King',
                'description' => 'Sauron sends a great army against Gondor. Gandalf arrives at Minas Tirith to warn Denethor of the attack, while Théoden musters the Rohirrim to ride to Gondor\'s aid. Minas Tirith is besieged. Denethor is deceived by Sauron and falls into despair. He burns himself alive on a pyre, nearly taking his son Faramir with him. Aragorn, accompanied by Legolas, Gimli and the Rangers of the North, takes the Paths of the Dead to recruit the Dead Men of Dunharrow, who are bound by a curse which denies them rest until they fulfil their ancient forsworn oath to fight for the King of Gondor.

Following Aragorn, the Army of the Dead strikes terror into the Corsairs of Umbar invading southern Gondor. Aragorn defeats the Corsairs and uses their ships to transport the men of southern Gondor up the Anduin, reaching Minas Tirith just in time to turn the tide of battle. Théoden\'s niece Éowyn, who joined the army in disguise, slays the Lord of the Nazgûl with help from Merry. Together, Gondor and Rohan defeat Sauron\'s army in the Battle of the Pelennor Fields, though at great cost. Théoden is killed, and Éowyn and Merry are wounded.',
                'author_id' => 1,
                'price' => 2350,
                'stock' => 2,
                'book_image' => '/images/books/the-return-of-the-king_1574618265.jpg',
                'created_at' => '2019-11-24 17:57:45',
                'updated_at' => '2019-11-24 18:43:14',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'The Fellowship of the Ring',
                'description' => 'After a failed attempt to cross the Misty Mountains over the Redhorn Pass, the Company take the perilous path through the Mines of Moria. They learn of the fate of Balin and his colony of Dwarves. After surviving an attack, they are pursued by Orcs and by a Balrog, an ancient fire demon. Gandalf faces the Balrog, and both of them fall into the abyss. The others escape and find refuge in the Elven forest of Lothlórien, where they are counselled by its rulers, Galadriel and Celeborn.

With boats and gifts from Galadriel, the Company travel down the River Anduin to the hill of Amon Hen. There, Boromir tries to take the Ring from Frodo, but Frodo puts it on and disappears. Frodo chooses to go alone to Mordor, but Sam guesses what he intends and goes with him.',
                'author_id' => 1,
                'price' => 1800,
                'stock' => 2,
                'book_image' => '/images/books/the-fellowship-of-the-ring_1574619551.jpg',
                'created_at' => '2019-11-24 18:19:11',
                'updated_at' => '2019-11-24 18:19:11',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'In Search of Lost time',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. At atque autem, cupiditate debitis deleniti distinctio doloribus facilis hic id ipsum magni minus nihil quia reiciendis repellat rerum ut velit veritatis?',
                'author_id' => 2,
                'price' => 900,
                'stock' => 1,
                'book_image' => NULL,
                'created_at' => '2019-11-24 18:23:24',
                'updated_at' => '2019-11-24 18:23:24',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'Ulysses',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. At atque autem, cupiditate debitis deleniti distinctio doloribus facilis hic id ipsum magni minus nihil quia reiciendis repellat rerum ut velit veritatis?',
                'author_id' => 3,
                'price' => 1725,
                'stock' => 10,
                'book_image' => NULL,
                'created_at' => '2019-11-24 18:23:51',
                'updated_at' => '2019-11-24 18:23:51',
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'Don Quixote',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. At atque autem, cupiditate debitis deleniti distinctio doloribus facilis hic id ipsum magni minus nihil quia reiciendis repellat rerum ut velit veritatis?',
                'author_id' => 4,
                'price' => 1945,
                'stock' => 5,
                'book_image' => NULL,
                'created_at' => '2019-11-24 18:24:27',
                'updated_at' => '2019-11-24 18:43:23',
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'War and Peace',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. At atque autem, cupiditate debitis deleniti distinctio doloribus facilis hic id ipsum magni minus nihil quia reiciendis repellat rerum ut velit veritatis?',
                'author_id' => 5,
                'price' => 1800,
                'stock' => 8,
                'book_image' => NULL,
                'created_at' => '2019-11-24 18:24:56',
                'updated_at' => '2019-11-24 18:49:19',
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'name' => 'Hamlet',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laborum nulla praesentium quis! Ad at, aut eligendi enim excepturi exercitationem facere magnam nulla obcaecati porro quidem quisquam sapiente, sed, sequi suscipit.',
                'author_id' => 6,
                'price' => 1000,
                'stock' => 3,
                'book_image' => NULL,
                'created_at' => '2019-11-24 18:26:36',
                'updated_at' => '2019-11-24 18:43:37',
                'deleted_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'name' => 'Anna Karenina',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laborum nulla praesentium quis! Ad at, aut eligendi enim excepturi exercitationem facere magnam nulla obcaecati porro quidem quisquam sapiente, sed, sequi suscipit.',
                'author_id' => 5,
                'price' => 1540,
                'stock' => 0,
                'book_image' => NULL,
                'created_at' => '2019-11-24 18:27:24',
                'updated_at' => '2019-11-24 18:46:40',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}