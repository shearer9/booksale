<?php

use Illuminate\Database\Seeder;

class AuthorsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('authors')->delete();
        
        \DB::table('authors')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'J.R.R. Tolkien',
                'birth_date' => '1892-01-03',
                'place_of_birth' => 'Bloemfontein',
                'created_at' => '2019-11-24 17:40:23',
                'updated_at' => '2019-11-24 17:40:23',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Marcel Proust',
                'birth_date' => '1950-01-01',
                'place_of_birth' => 'New York',
                'created_at' => '2019-11-24 18:20:30',
                'updated_at' => '2019-11-24 18:20:30',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'James Joyce',
                'birth_date' => '1955-05-01',
                'place_of_birth' => 'Miami',
                'created_at' => '2019-11-24 18:21:01',
                'updated_at' => '2019-11-24 18:21:01',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Miguel de Cervantes',
                'birth_date' => '1966-10-01',
                'place_of_birth' => 'Madrid',
                'created_at' => '2019-11-24 18:21:28',
                'updated_at' => '2019-11-24 18:21:28',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Leo Tolstoj',
                'birth_date' => '1940-10-01',
                'place_of_birth' => 'Moskva',
                'created_at' => '2019-11-24 18:22:00',
                'updated_at' => '2019-11-24 18:22:00',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'William Shakespeare',
                'birth_date' => '1955-01-01',
                'place_of_birth' => 'UK',
                'created_at' => '2019-11-24 18:26:05',
                'updated_at' => '2019-11-24 18:26:05',
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'George Orwell',
                'birth_date' => '1933-09-01',
                'place_of_birth' => 'England',
                'created_at' => '2019-11-24 18:28:03',
                'updated_at' => '2019-11-24 18:28:03',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}