@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-7 shadow p-3 bg-info rounded">
                <h2 class="text-center my-4">New Author</h2>
            </div>
            <form method="POST" action="/authors" class="col-7 shadow p-3 mb-5 bg-white rounded">
                @csrf
                <div class="form-group">
                    <label for="name">Name:</label>
                    <input type="text" class="form-control" id="name" name="name">
                </div>
                <div class="form-group">
                    <label for="birth_date">Birth date:</label>
                    <input type="date" step="0.01" class="form-control" id="birth_date" name="birth_date">
                </div>
                <div class="form-group">
                    <label for="place_of_birth">Place of birth:</label>
                    <input type="text" class="form-control" id="place_of_birth" name="place_of_birth">
                </div>
                <div class="d-flex justify-content-end">
                    <button type="submit" class="btn btn-primary">Confirm</button>
                </div>
            </form>
            <div class="col-7">
                @include('layouts.errors')
            </div>
        </div>
    </div>
@endsection
