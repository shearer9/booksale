@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-start">
            <table class="table table-hover">
                <caption>List of authors</caption>
                <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Birth date</th>
                    <th scope="col">Place of birth</th>
                    <th scope="col" style="padding-left: 5em;">Actions</th>
                </tr>
                </thead>
                <tbody>
                <div class="modal fade" id="modalBooks" tabindex="-1" role="dialog"
                     aria-labelledby="exampleModalCenterTitle"
                     aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5>Books from this author</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body list-group list-group-flush" v-if="authorsBooks.length">
                                <a v-for="book in authorsBooks" :href="'/books/' + book.id"
                                   class="font-weight-bold list-group-item list-group-item-action">
                                    @{{book.name}}
                                </a>
                            </div>
                            <p class="modal-body" v-else>
                                Unfortunately we do not have any books from this author. Would you like to check some
                                other authors?
                            </p>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>

                @foreach($authors as $author)
                    <tr>
                        <th scope="row">{{$loop->iteration}}</th>
                        <td>{{$author->name}}</td>
                        <td>{{$author->birth_date}}</td>
                        <td>{{$author->place_of_birth}}</td>
                        <td>
                            <div class="row">
                                <button @click="getAuthorsBooks({{$author->id}})" type="button"
                                        class="btn btn-sm btn-info mx-2" data-toggle="modal"
                                        data-target="#modalBooks"
                                @if(!Auth::user()->isAdmin())
                                        style="margin-left: 6em!important;"
                                        @endif
                                    >Books
                                </button>
{{--                                <a href="/books?author={{$author->id}}" class="btn btn-sm btn-primary mx-2"--}}
{{--                                   @if(!Auth::user()->isAdmin())--}}
{{--                                   style="margin-left: 6em!important;"--}}
{{--                                        @endif>--}}
{{--                                    Books</a>--}}
                                @admin
                                <a href="/authors/{{$author->id}}/edit" class="btn btn-sm btn-dark mx-2">Edit</a>
                                <form id="authorDelete-{{$author->id}}" method="POST"
                                      action="/authors/{{$author->id}}">
                                    @csrf
                                    @method('DELETE')
                                    @component('layouts.modal')
                                        @slot('id')
                                            {{$author->id}}
                                        @endslot
                                        @slot('item')
                                            author
                                        @endslot
                                    @endcomponent
                                    <button type="button" class="btn btn-sm btn-danger mx-2" data-toggle="modal"
                                            data-target="#modalDelete-{{$author->id }}">Delete
                                    </button>
                                </form>
                                @endAdmin
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="d-flex justify-content-between my-3">
            {{ $authors->links() }}
            @admin
            <a class="btn btn-primary h-50" href="/authors/create">Add author</a>
            @endAdmin
        </div>
    </div>
@endsection

