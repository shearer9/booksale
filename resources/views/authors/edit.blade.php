@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-7 shadow p-3 bg-info rounded">
                <h2 class="text-center my-4">Edit Author</h2>
            </div>
            <form method="post" action="/authors/{{$author->id}}" class="col-7 shadow p-3 mb-5 bg-white rounded">
                @csrf
                @method('PATCH')
                <div class="form-group">
                    <label for="name">Name:</label>
                    <input type="text" class="form-control" id="name" name="name" value="{{$author->name}}">
                </div>
                <div class="form-group">
                    <label for="birth_date">Birth date:</label>
                    <input type="date" class="form-control" id="birth_date" name="birth_date" value="{{$author->birth_date}}">
                </div>
                <div class="form-group">
                    <label for="place_of_birth">Place of birth:</label>
                    <input type="text" class="form-control" id="place_of_birth" name="place_of_birth" value="{{$author->place_of_birth}}">
                </div>
                <div class="d-flex justify-content-end">
                    <button type="submit" class="btn btn-primary">Confirm</button>
                </div>
            </form>
            <div class="col-7">
                @include('layouts.errors')
            </div>
        </div>
    </div>
@endsection
