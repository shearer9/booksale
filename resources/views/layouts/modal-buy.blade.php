<div class="modal fade" id="modalBuy-{{ $book->id }}" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title book-title">{{$book->name}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Do you want to buy this book?
                <hr>
                Price: {{$book->price}}€
                <br>
                Balance after purchase: {{number_format(Auth::user()->balance - $book->price, 2, '.', '')}}€
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" form_id="bookBuy-{{ $book->id }}">Accept</button>
            </div>
        </div>
    </div>
</div>