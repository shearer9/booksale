@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-start">
            <table class="table table-hover">
                <caption>List of transactions</caption>
                <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">User name</th>
                    <th scope="col">Book name</th>
                    <th scope="col">Price</th>
                    <th scope="col">Amount</th>
                    <th scope="col">Date</th>
                </tr>
                </thead>
                <tbody>
                @foreach($transactions as $transaction)
                    <tr>
                        <th scope="row">{{$loop->iteration}}</th>
                        <td>{{$transaction->user->name}}</td>
                        @if($transaction->book)
                        <td>{{$transaction->book->name}}</td>
                        <td>{{$transaction->book->price}}€</td>
                        <td></td>
                        <td>{{$transaction->created_at}}</td>
                        @else
                        <td></td>
                        <td></td>
                        <td>{{$transaction->amount}}€</td>
                        <td>{{$transaction->created_at}}</td>
                        @endif
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="d-flex justify-content-end">
            {{ $transactions->links() }}
        </div>
    </div>
@endsection

