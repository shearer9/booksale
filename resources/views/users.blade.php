@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-start">
            <div class="modal fade" id="modalBalance" tabindex="-1" role="dialog"
                 aria-labelledby="exampleModalCenterTitle"
                 aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <form id="addBalance" method="POST" action="/transactions/add_balance">
                        @csrf
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">

                                <label for="add-balance">Add balance</label>
                                <input type="number" step="0.01" min="0" class="form-control" id="add-balance" name="amount" placeholder="€">
                                <input type="hidden" name="user_id" :value="user_id">
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary" form_id="addBalance">Confirm</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <table class="table table-hover">
                <caption>List of users</caption>
                <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Phone</th>
                    <th scope="col">E-mail</th>
                    <th scope="col">Balance</th>
                    <th scope="col">Registration</th>
                    <th scope="col">Actions</th>
                </tr>
                </thead>
                <tbody>

                @foreach($users as $user)
                    <tr>
                        <th scope="row">{{$loop->iteration}}</th>
                        <td>{{$user->name}}</td>
                        <td>{{$user->phone}}</td>
                        <td>{{$user->email}}</td>
                        <td>{{$user->balance}}€</td>
                        <td>{{$user->created_at}}</td>
                        <td>
                            <div class="row">
                                <button @click="user_id = {{$user->id}}"
                                        type="button"
                                        class="btn btn-sm btn-info" data-toggle="modal"
                                        data-target="#modalBalance"
                                >Add balance
                                </button>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="my-3">
                {{ $users->links() }}
            </div>
        </div>
    </div>
@endsection

