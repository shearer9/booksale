@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-start">
            @if($books->isEmpty())
                <div class="d-flex align-items-center shadow col-12 my-3 justify-content-center">
                    <p class="text-center">Unfortunately we do not have any books right now.</p>
                </div>
                    @endif
            @foreach($books as $book)
            <div class="col-3 card text-center" style="width: 18rem;">
                <div class="card-body">
                    <h3 class="card-title book-title">{{$book->name}}</h3>
                    <p class="card-text">{{$book->author->name}}</p>
                    <small>Stock: {{$book->stock}}</small>
                    <hr>
                    <p class="card-text">{{$book->price}}€</p>
                    <a type="button" class="btn btn-info" href="/books/{{$book->id}}">Details</a>
                </div>
            </div>
            @endforeach
        </div>
        <div class="d-flex justify-content-between my-3">
            {{ $books->links() }}
            @admin
            <a class="btn btn-primary h-50" href="/books/create">Add book</a>
            @endAdmin
        </div>
    </div>
@endsection

