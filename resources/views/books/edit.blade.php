@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-7 shadow p-3 bg-info rounded">
                <h2 class="text-center my-4">Edit Book</h2>
            </div>
            <form method="post" action="/books/{{$book->id}}" enctype="multipart/form-data" class="col-7 shadow p-3 mb-5 bg-white rounded">
                @csrf
                @method('PATCH')
                <div class="form-group">
                    <label for="name">Name:</label>
                    <input type="text" class="form-control" id="name" name="name" value="{{$book->name}}">
                </div>
                <div class="form-group">
                    <label for="author_id">Author:</label>
                    <select class="form-control" id="author_id" name="author_id">
                            <option value="{{$book->author->id}}" selected>{{$book->author->name}}</option>
                            @foreach($authors as $author)
                            <option value={{$author->id}}>{{$author->name}}</option>
                            @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="description">Description:</label>
                    <textarea class="form-control" id="description" name="description" rows="3">{{$book->description}}</textarea>
                </div>
                <div class="form-group">
                    <label for="price">Price:</label>
                    <input type="number" step="0.01" class="form-control" id="price" name="price" value="{{$book->price}}">
                </div>
                <div class="form-group">
                    <label for="stock">Stock:</label>
                    <input type="number" class="form-control" id="stock" name="stock" value="{{$book->stock}}">
                </div>
                <div class="form-group">
                    <label for="book_image" class="col-form-label">Book cover:</label>
                    <input id="book_image" type="file" class="form-control" name="book_image">
                </div>
                <div class="d-flex justify-content-end">
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </form>
            <div class="col-7">
                @include('layouts.errors')
            </div>
        </div>
    </div>
@endsection
