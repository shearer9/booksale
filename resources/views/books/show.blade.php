@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="card col-7 p-0 bg-light">
                @if($book->book_image)
                    <img src="{{ asset($book->book_image) }}" style="width: 18rem; height: 14rem;"
                         class="d-flex card-img-top img-fluid img-thumbnail mx-auto rounded">
                @endif
                <div class="card-body">
                    <h5 class="card-title text-center book-title">{{$book->name}}</h5>
                    <p class="card-text"><b>Description: </b>{{$book->description}}</p>
                    <p class="card-text"><b>Price: </b>{{$book->price}}</p>
                    <p class="card-text">
                        @if($book->stock < 1)
                            <b>Stock: </b>Coming soon
                        @else
                            <b>Stock: </b>{{$book->stock}}
                        @endif
                    </p>
                    <div class="d-flex justify-content-end">
                        @if(Auth::user()->isAdmin())
                            <a href="/books/{{$book->id}}/edit" class="btn btn-dark mx-2">Edit</a>
                            <form id="bookDelete-{{ $book->id }}" method="POST" action="/books/{{$book->id}}">
                                @csrf
                                @method('DELETE')
                                @component('layouts.modal')
                                    @slot('id')
                                        {{$book->id}}
                                    @endslot
                                    @slot('item')
                                    book
                                    @endslot
                                    @endcomponent
                                <button type="button" class="btn btn-danger" data-toggle="modal"
                                        data-target="#modalDelete-{{$book->id }}">Delete
                                </button>
                            </form>
                        @else
                            @if($book->stock < 1)
                                <button type="button" class="btn btn-outline-secondary" disabled>Coming soon</button>
                            @elseif((Auth::user()->balance < $book->price))
                                <button type="button" class="btn btn-outline-secondary" disabled>No credit</button>
                            @else
                                <button type="button" class="btn btn-primary" data-toggle="modal"
                                        data-target="#modalBuy-{{$book->id }}">Buy book
                                </button>
                                <form id="bookBuy-{{ $book->id }}" method="POST" action="/books/{{$book->id}}/buy">
                                    @csrf
                                    @include('layouts.modal-buy')
                                </form>
                            @endif
                        @endif
                    </div>
                </div>
                @include('layouts.errors')
                @if (Session::has('msg'))
                    <div class=" mx-3 px-3 alert alert-success text-center" role="alert">
                        {{Session::get('msg')}}
                    </div>
                @endif
                <div class="card-body text-muted d-flex">
                    <a href="/?author={{$book->author->id}}">
                        {{$book->author->name}}
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection
