# Booksaler

Application for selling books.

## Installation

```bash
# install dependencies
composer install

# create database

# edit .env
rename .env.example to .env and set right credentials
(DB_DATABASE, DB_USERNAME, DB_PASSWORD)
#Application can send mails so be sure to configure email settings
#You can use this email for testing purposes 
MAIL_DRIVER=smtp
MAIL_HOST=smtp.googlemail.com
MAIL_PORT=465
MAIL_USERNAME=spark.mailforum@gmail.com
MAIL_PASSWORD=Spark1-1
MAIL_ENCRYPTION=ssl

# set application key for encryption
php artisan key:generate

# fill database
php artisan migrate --seed

#build frontend
npm run watch

# serve file
php artisan serve
```

## Usage

There are 2 types of users. Admin who is pre-configured and regular user.
Admin can CRUD books, authors and has insight in all transactions happening in application. He is also responsible for adding balance to other users.
Regular user can buy books if he has enough balance. He will get confirmation mail after purchase.
There is option to see books only from selected author with a AJAX call using Axios and Vue.

admin@admin.com    12345678

test accounts: test@test.com    12345678

test2@test.com just increment number, same password



## License
[MIT](https://choosealicense.com/licenses/mit/)