<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class SendBookConfirmationMailListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $to_name = $event->user->name; //  'Ivan'; // $user->name
        $to_email = $event->user->email;   // 'ico.starmo@gmail.com'; //$user->email
        $data = ['name'=>$to_name, 'body' => 'You have successfully purchased the book - ' . $event->book->name];
        Mail::send('layouts.mail', $data, function($message) use ($to_name, $to_email) {
            $message->to($to_email, $to_name)
                ->subject('Book purchase');
            $message->from(env('MAIL_USERNAME'), 'Booksaler');
        });
    }
}
