<?php

namespace App\Http\Controllers;

use App\Book;
use App\Author;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Traits\UploadTrait;


class BookController extends Controller
{
    use UploadTrait;

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin', ['except' => ['index', 'show']]);
    }

    public function index()
    {
        $this->validate(request(), [
            'author' => 'integer'
        ]);
        $query = request()->query('author');
        if ($query) {
            $books = Book::where('author_id', $query)->with('author')->paginate(8);
            return view('books.index', ['books' => $books]);
        }
        $books = Book::with('author')->paginate(8);
        return view('books.index', ['books' => $books]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $authors = Author::all();
        return view('books.create', ['authors' => $authors]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(request(), [
            'name' => 'required|string',
            'description' => 'required|string',
            'author_id' => 'required|integer',
            'price' => 'required|numeric',
            'stock' => 'required|integer',
            'book_image' => 'nullable|image|mimes:jpeg,png,jpg|max:2048'
        ]);

        $book = Book::make(request([
            'name',
            'description',
            'author_id',
            'price',
            'stock'
        ]));

        if ($request->has('book_image')) {
            // Get image file
            $image = $request->file('book_image');
            // Make a image name based on book name and current timestamp
            $name = Str::slug($request->name) . '_' . time();
            // Define folder path
            $folder = '/images/books/';
            // Make a file path where image will be stored [ folder path + file name + file extension]
            $filePath = $folder . $name . '.' . $image->getClientOriginalExtension();
            // Upload image
            $this->uploadOne($image, $folder, 'public', $name);
            // Set image path in database to filePath
            $book->fill(['book_image' => $filePath]);
            $book->save();
            return redirect('/books/' . $book->id);
        }
        $book->save();
        return redirect('/books/' . $book->id);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Book $book
     * @return \Illuminate\Http\Response
     */
    public function show(Book $book)
    {
        return view('books.show', ['book' => $book]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Book $book
     * @return \Illuminate\Http\Response
     */
    public function edit(Book $book)
    {
        $authors = Author::all()->except($book->author->id);
        return view('books.edit', ['book' => $book, 'authors' => $authors]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Book $book
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Book $book)
    {
        $this->validate(request(), [
            'name' => 'required|string',
            'description' => 'required|string',
            'author_id' => 'required|integer',
            'price' => 'required|numeric',
            'stock' => 'required|integer',
            'book_image' => 'nullable|image|mimes:jpeg,png,jpg|max:2048'
        ]);

        $book->update(request([
            'name',
            'description',
            'author_id',
            'price',
            'stock'
        ]));

        if ($request->has('book_image')) {
            // Get image file
            $image = $request->file('book_image');
            // Make a image name based on book name and current timestamp
            $name = Str::slug($request->name) . '_' . time();
            // Define folder path
            $folder = '/images/books/';
            // Make a file path where image will be stored [ folder path + file name + file extension]
            $filePath = $folder . $name . '.' . $image->getClientOriginalExtension();
            // Upload image
            $this->uploadOne($image, $folder, 'public', $name);
            // Set image path in database to filePath
            $book->book_image = $filePath;
            $book->save();
        }

        return redirect('/books/' . $book->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Book $book
     * @return \Illuminate\Http\Response
     */
    public function destroy(Book $book)
    {
        $book->delete();
        return redirect('/books');
    }
}
