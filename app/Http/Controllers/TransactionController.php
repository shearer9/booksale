<?php

namespace App\Http\Controllers;

use App\Book;
use App\Events\AddedBalanceToUserEvent;
use App\Events\BuyingBookEvent;
use App\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use App\User;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin', ['except' => ['buyBook']]);
        $this->middleware('user', ['only' => ['buyBook']]);
    }

    public function index()
    {
        $transactions = Transaction::with('user', 'book')->paginate(10);
        return view('transactions.index', ['transactions' => $transactions]);
    }

    public function showUsers()
    {
        $users = User::where('is_admin', false)->paginate(10);
        return view('users', ['users' => $users]);
    }

    public function buyBook(Book $book)
    {
        if (Auth::user()->balance < $book->price || $book->stock < 1) {
            return Redirect::back()->withErrors('You are not allowed');
        }
        $transaction = Transaction::create(['user_id' => Auth::user()->id, 'book_id' => $book->id]);
        event(new BuyingBookEvent($transaction));
        return redirect()->back()->with(['msg' => 'You have successfully purchased the book !']);
    }

    public function addBalance(Request $request)
    {
        $this->validate(request(), [
            'amount' => 'required|numeric|between:0.00,999.99',
            'user_id' => 'required|integer'
        ]);
        $user = User::findOrFail($request->user_id);
        $amount = $request->amount;
        $transaction = Transaction::create([
            'user_id' => $user->id,
            'amount' => $amount
        ]);
        event(new AddedBalanceToUserEvent($transaction));
        return redirect()->back();
    }
}
