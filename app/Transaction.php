<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Transaction extends Model
{
    protected $fillable = [
        'user_id', 'book_id', 'amount'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function book(){
        return $this->belongsTo(Book::class)->withTrashed();
    }
}
