<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\Transaction;

class BuyingBookEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $user;
    public $book;

    public function __construct(Transaction $transaction)
    {
        $this->user = $transaction->user;
        $this->book = $transaction->book;
    }
}
