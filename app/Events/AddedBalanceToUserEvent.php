<?php

namespace App\Events;

use App\Transaction;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class AddedBalanceToUserEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $user;
    public $amount;

    public function __construct(Transaction $transaction)
    {
        $this->user = $transaction->user;
        $this->amount = $transaction->amount;
    }
}
