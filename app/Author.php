<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Author extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name', 'birth_date', 'place_of_birth'
    ];

    public function books()
    {
        return $this->hasMany(Book::class);
    }
}
